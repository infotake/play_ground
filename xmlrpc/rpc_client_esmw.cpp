#include <string>
#include <iostream>
#include <xmlrpc-c/girerr.hpp>
#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/client_simple.hpp>

#include <sys/types.h>
#include <sys/sysctl.h>

int get_rpc_port(bool local){
	size_t size;
	int buf, ret;
	size = sizeof(buf);
	if (local)  ret = sysctlbyname("qess.cf.esmw_self", &buf, &size, NULL, 0);
	else  ret = sysctlbyname("qess.cf.esmw_peer", &buf, &size, NULL, 0);
	if (ret<0){
		perror("get sysctl fail");
		return 9823;
	}
	return buf;
}

int get_local_rpc_port(){
	return get_rpc_port(true);
}

int get_remote_rpc_port(){
	return get_rpc_port(false);
}

int main(int argc, char* argv[])
{
	std::cout<<get_local_rpc_port()<<std::endl;
	std::cout<<get_remote_rpc_port()<<std::endl;
	try
	{
		std::string const serverUrl("http://localhost:9823");
		std::string const methodName("test");
		xmlrpc_c::clientSimple myClient;
		xmlrpc_c::value result;
		myClient.call(serverUrl, methodName, "", &result);

		std::string msg = xmlrpc_c::value_string(result);
		std::cout<<msg<<std::endl;
	}
	catch (std::exception const& e)
	{
		std::cerr << "Client threw error: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cerr << "Client threw unexpected error." << std::endl;
	}
	return 0;
}