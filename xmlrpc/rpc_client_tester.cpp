#include <string>
#include <iostream>
#include <xmlrpc-c/girerr.hpp>
#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/client_simple.hpp>

#include <sys/types.h>
#include <sys/sysctl.h>

int get_rpc_port(){
	return 9487;
}

int main(int argc, char* argv[])
{
	std::cout<<get_rpc_port()<<std::endl;
	try
	{
		std::string const serverUrl("http://localhost:9487");
		std::string const methodName("test");
		xmlrpc_c::clientSimple myClient;
		xmlrpc_c::value result;
		myClient.call(serverUrl, methodName, "ii", &result);

		std::string msg = xmlrpc_c::value_string(result);
		std::cout<<msg<<std::endl;
	}
	catch (std::exception const& e)
	{
		std::cerr << "Client threw error: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cerr << "Client threw unexpected error." << std::endl;
	}
	return 0;
}