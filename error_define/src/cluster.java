
public enum cluster {
    DATABASE(0 , "A database error has occured."),
    DUPLICATE_USER(1 , "This user already exists.");


    private final int code;
    private final String description;
    private final int base_id = 10000;

    private cluster(int code, String description) {
        this.code = base_id + code;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code + ": " + description;
    }

}

