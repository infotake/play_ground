class LogParser:

    def __init__(self):
        self.register_obj_list = []

    def add(self, obj):
        self.register_obj_list.append(obj)

    def start(self):
        for obj in self.register_obj_list:
            print obj.start_parse()


if __name__ == '__main__':
    from module import ha

    log_parser = LogParser()

    ha_log = ha.HAParser()
    ha_log.set_folders(['./log/'])
    ha_log.add_parser(ha.HAParser.parser1)
    ha_log.add_parser(ha.HAParser.parser2, 'ha2')

    log_parser.add(ha_log)





    log_parser.start()



