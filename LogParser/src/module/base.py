import glob
import bz2


class ParserBase:

    def __init__(self):
        self.parser_list = []
        self.folder_list = []
        self.output_folder = './tmp/'
        self.output_file = "%s.log" % (self.output_folder + __file__.split('/')[-1].split('.')[0])
        self.parser_log_file = {}

    def set_default_output_file(self, filename):
        if not filename.endswith(".log"):
            filename = "%s.log" % filename
        self.output_file = self.output_folder + filename

    def set_folders(self, folder_list):
        self.folder_list = folder_list

    def set_output_folder(self, folder):
        self.output_folder = folder

    # Entry Function
    def do_parse(self, line):
        ret = ''
        for parse_func in self.parser_list:
            tmp_ret = parse_func(line)
            if tmp_ret:
                log_file = self.parser_log_file[parse_func]
                ret = ret + ("%s+=+" % log_file) + tmp_ret
        return ret

    def start_parse(self):
        file_list_log = []
        file_list_bz2 = []

        for folder_path in self.folder_list:
            file_list_log = glob.glob('%s/*.log' % folder_path)

        for folder_path in self.folder_list:
            file_list_bz2 = glob.glob('%s/*.bz2' % folder_path)

        ret_list = ''

        for file_name in file_list_log + file_list_bz2:
            if file_name.endswith('bz2'):
                for line in bz2.BZ2File(file_name, "r").readlines():
                    ret_line = self.do_parse(line.strip())
                    if ret_line:
                        ret_list = ret_list + '\n' + ret_line
            else:
                with open(file_name, 'r') as in_file:
                    for line in in_file.readlines():
                        ret_line = self.do_parse(line.strip())
                        if ret_line:
                            ret_list = ret_list + '\n' + ret_line
        if ret_list:

            log_file_content_map = {}

            for line in ret_list.splitlines():
                line = line.strip()
                if line:
                    content = line.split("+=+")
                    if content[0] in log_file_content_map:
                        log_file_content_map[content[0]] += '\n%s' % content[1]
                    else:
                        log_file_content_map[content[0]] = content[1]

            for log_file_name in log_file_content_map:
                with open(log_file_name, 'w') as out_file:
                    out_file.write(log_file_content_map[log_file_name])

    def add_parser(self, parse_func, log_file=None):
        self.parser_list.append(parse_func)
        if not log_file:
            log_file = self.output_file
        else:
            if not log_file.endswith('.log'):
                log_file = self.output_folder + "%s.log" % log_file
            else:
                log_file = self.output_folder + log_file
        self.parser_log_file[parse_func] = log_file
