
from base import ParserBase


class HAParser(ParserBase):

    @staticmethod
    def parser1(line):
        if "mount" in line:
            return line
        return None

    @staticmethod
    def parser2(line):
        if 'g_dev_orphan' in line:
            return line
        return None
