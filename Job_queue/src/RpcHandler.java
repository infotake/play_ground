import java.util.Queue;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Supplier;

public class RpcHandler {

    private Queue<Supplier<QnapResult>> GlobalFuncQueue;

    public  RpcHandler(){
        this.GlobalFuncQueue = new LinkedBlockingQueue<>();
    }

    public QnapResult do_test_1(QnapInput in_data){
        // Do enqueue
        Supplier<QnapResult> ret = () -> new RpcFunc().do_test_1(in_data);
        this.GlobalFuncQueue.add(ret);
        return new QnapResult();
    }

    public QnapResult do_test_2(QnapInput in_data){
        Supplier<QnapResult> ret = () -> new RpcFunc().do_test_1(in_data);
        this.GlobalFuncQueue.add(ret);
        return new QnapResult();
    }

    public QnapResult do_test_3(QnapInput in_data){
        Supplier<QnapResult> ret = () -> new RpcFunc().do_test_1(in_data);
        this.GlobalFuncQueue.add(ret);
        return new QnapResult();
    }

    public QnapResult do_test_4(QnapInput in_data){
        Supplier<QnapResult> ret = () -> new RpcFunc().do_test_1(in_data);
        this.GlobalFuncQueue.add(ret);
        return new QnapResult();
    }


    public static void main(String[] args) {

        RpcHandler rpcHandler = new RpcHandler();
        rpcHandler.do_test_1(new QnapInput(101, "msg_1") );
        rpcHandler.do_test_3(new QnapInput(103, "msg_3") );
        rpcHandler.do_test_4(new QnapInput(104, "msg_4") );
        rpcHandler.do_test_2(new QnapInput(102, "msg_2") );


        while( rpcHandler.GlobalFuncQueue.size() >0 ){
            System.out.println("print queue");
            System.out.println(rpcHandler.GlobalFuncQueue.poll().get());

        }



    }

}
