import java.util.concurrent.BlockingQueue;
import java.util.function.Supplier;

public class Consumer implements Runnable{

        private BlockingQueue<Supplier<QnapResult>> queue;

        public Consumer(BlockingQueue<Supplier<QnapResult>> q){
            this.queue=q;
        }

        @Override
        public void run() {
            try{
                //consuming messages until exit message is received
                while(true){
                    System.out.println(queue.take().get());
                    System.out.println("Consumed again");

                }
            }catch(InterruptedException e) {
                e.printStackTrace();
            }
        }

}
