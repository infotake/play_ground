

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.function.Supplier;

class singleQueueClassTest extends ThreadQueue{

    public static BlockingQueue<Supplier<QnapResult>> global_queue = new ArrayBlockingQueue<>(1000);
    public singleQueueClassTest(){
    }
    public static void start(){
        startConsumer(1);
    }


    public static void main(String[] args) {
        singleQueueClassTest queue = new singleQueueClassTest();

        Supplier<QnapResult> job1 = () -> new QnapResult(1,"test1");
        System.out.println("enqueue 1");
        queue.enQueue(job1);
        System.out.println("dequeue 1");
        Supplier<QnapResult> job2 = () -> new QnapResult(2,"test2");
        System.out.println("enqueue 2");
        queue.enQueue(job2);
        System.out.println("dequeue 2");
        Supplier<QnapResult> job3 = () -> new QnapResult(3,"test3");
        System.out.println("enqueue 3");
        queue.enQueue(job3);
        System.out.println("dequeue 3");
        queue.start();

    }



}
