public class QnapResult {

    public int retCode;
    public String retMsg;

    public QnapResult(){
        this.retCode = 0;
        this.retMsg = "";
    }

    public QnapResult(int code, String msg){
        this.retCode = code;
        this.retMsg = msg;
    }

    @Override
    public String toString() {
        return "result:" + this.retCode + ", " + this.retMsg;
    }

    public void setRetCode(int retCode) {
        this.retCode = retCode;
    }
    public void setRetMsg(String msg){
        this.retMsg = msg;
    }
}
