public class QnapInput {

    public int inputCode;
    public String inputMsg;

    public QnapInput(){
        this.inputCode = 0;
        this.inputMsg = "";
    }

    public QnapInput(int code, String msg){
        this.inputCode = code;
        this.inputMsg = msg;
    }

    public void setInputCode(int inputCode) {
        this.inputCode = inputCode;
    }
    public void setInputMsg(String msg){
        this.inputMsg = msg;
    }
}
